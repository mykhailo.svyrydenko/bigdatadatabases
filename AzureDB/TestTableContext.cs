﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace AzureDB
{
    public class TestTableContext:DbContext
    {
        public TestTableContext() : base("TestDB")
        { }

        public DbSet<TestTable> testTable { get; set; }
        
    }
}
