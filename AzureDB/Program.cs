﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;


namespace AzureDB
{
    class Program
    {
        static void Main(string[] args)
        {
            TestTableContext db = new TestTableContext();
            
            TestTable testTable = new TestTable()
            {                
                CampaignName = "dddd",
                EnableCampaign = false,
                ContentFrequancy = 44,
                ShowTime = DateTime.Now
            };
            db.testTable.Add(testTable);
            db.SaveChanges();
            var query = db.Database.ExecuteSqlCommand("create table TestTable2 (id int primary key identity, name varchar(255))");
            db.SaveChanges();
            Console.WriteLine("___");
            Console.ReadLine();
        }
    }
}