﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace AzureDB
{
    [Table("TestTable")]
    public class TestTable
    {
        [Key]
        public int Id { get; set; }
        public string CampaignName { get; set; }
        public DateTime ShowTime { get; set; }
        public bool EnableCampaign { get; set; }
        public float ContentFrequancy { get; set; }
    }
}
