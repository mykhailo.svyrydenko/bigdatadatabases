﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MomgoDBServer
{
    class Program
    {
        static void Main(string[] args)
        {
            // or, to connect to a replica set, with auto-discovery of the primary, supply a seed list of members
            var client = new MongoClient("mongodb://localhost:27017");
            var database = client.GetDatabase("TestDB");
            var collection = database.GetCollection<BsonDocument>("TestCollection");
            List<BsonDocument> zz = new List<BsonDocument>();
            for (int j = 0; j < 50; j++)
            {
                zz = new List<BsonDocument>();
                for (int i = 0; i < 100000; i++)
                {
                    var document = new BsonDocument
                {
                    { "name", "MongoDB" },
                    { "type", "Database" },
                    { "count1", i }
                };

                    zz.Add(document);
                    //collection.InsertOne(document);
                }
                collection.InsertManyAsync(zz);
                //collection.InsertMany(zz);
                Console.WriteLine(j + " posted");
            }
            var z = collection.Find(
               new BsonDocument
                {
                    { "name", "MongoDB" },
                    { "type", "Database" },
                    { "count1", 345 }
                }).First();
            var m = collection.Find(
                new BsonDocument
                {
                    { "name", "MongoDB" },
                    { "type", "Database" },
                    { "count1", 345 }
                });
            Console.WriteLine(z.ToString());
            Console.WriteLine("___________");
            Console.WriteLine(m.CountDocuments());
            Console.ReadLine();
        }
    }
}
