﻿using InfluxDB.Collector;
using LibInfluxDB.Net;
using LibInfluxDB.Net.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfluxDB_InfluxDBdotNET
{//http://localhost:8086
    class Program
    {
        static void Main(string[] args)
        {
            Collect().Wait();
        }

        async static Task Collect()
        {
            var process = Process.GetCurrentProcess();

            Metrics.Collector = new CollectorConfiguration()
                .Tag.With("host", Environment.GetEnvironmentVariable("COMPUTERNAME"))
                .Tag.With("os", Environment.GetEnvironmentVariable("OS"))
                .Tag.With("process", Path.GetFileName(process.MainModule.FileName))
                .Batch.AtInterval(TimeSpan.FromSeconds(2))
                .WriteTo.InfluxDB("http://localhost:8086", "mydb")
                .CreateCollector();

            while (true)
            {
                Metrics.Increment("iterations2");

                Metrics.Write("cpu_time",
                    new Dictionary<string, object>
                    {
                        { "value", 1 },
                        { "user", 2}
                    });

                Metrics.Measure("working_set", process.WorkingSet64);

                await Task.Delay(1000);
            }
        }
    }
}
