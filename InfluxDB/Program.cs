﻿using InfluxDB.Collector;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Vibrant.InfluxDB.Client;

namespace InfluxDB
{
    class Program
    {
        public class ComputerInfo
        {
            [InfluxTimestamp]
            public DateTime Timestamp { get; set; }

            [InfluxTag("host1")]
            public string Host { get; set; }

            [InfluxTag("region1")]
            public string Region { get; set; }

            [InfluxField("cpu1")]
            public double CPU { get; set; }

            [InfluxField("ram1")]
            public long RAM { get; set; }
        }
        private static ComputerInfo[] CreateTypedRowsStartingAt(DateTime start, int rows)
        {
            var rng = new Random();
            var regions = new[] { "west-eu", "north-eu", "west-us", "east-us", "asia" };
            var hosts = new[] { "some-host", "some-other-host" };

            var timestamp = start;
            var infos = new ComputerInfo[rows];
            for (int i = 0; i < rows; i++)
            {
                long ram = rng.Next(int.MaxValue);
                double cpu = rng.NextDouble();
                string region = regions[rng.Next(regions.Length)];
                string host = hosts[rng.Next(hosts.Length)];

                var info = new ComputerInfo { Timestamp = timestamp, CPU = cpu, RAM = ram, Host = host, Region = region };
                infos[i] = info;

                timestamp = timestamp.AddSeconds(1);
                //Console.WriteLine("Timestamp:"+info.Timestamp+" CPU:"+info.CPU+" RAM:"+info.RAM+" HOST:"+info.Host+" Region:"+info.Region);
            }

            return infos;
        }

        public static async Task Should_Write_Typed_Rows_To_Database()
        {
            var client = new InfluxClient(new Uri("http://localhost:8086"));
            var infos = CreateTypedRowsStartingAt(new DateTime(2010, 1, 1, 1, 1, 1, DateTimeKind.Utc), 25000);
            await client.WriteAsync("mydb", "q5", infos);

        }
        static void Main(string[] args)
        {
            for (int i = 0; i < 40; i++)
            {
                Should_Write_Typed_Rows_To_Database();
                Thread.Sleep(3050);
                Console.WriteLine("done:" + i);
            }

            Console.ReadLine();
        }
        public void InfluxDBCollectorMethod()
        {
            var process = Process.GetCurrentProcess();
            Metrics.Collector = new CollectorConfiguration()
               .Tag.With("host", Environment.GetEnvironmentVariable("COMPUTERNAME"))
               .Batch.AtInterval(TimeSpan.FromSeconds(2))
               .WriteTo.InfluxDB("http://192.168.99.100:8086", "mydb")
               .CreateCollector();
            Metrics.Increment("iterations");

            Metrics.Write("cpu_time",
                new Dictionary<string, object>
                {
                    { "value", process.TotalProcessorTime.TotalMilliseconds },
                    { "user", process.UserProcessorTime.TotalMilliseconds }
                });

            Metrics.Measure("working_set", process.WorkingSet64);
        }
    }
}
